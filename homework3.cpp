#include <iostream>
#include <cstdlib>
#include <vector>
#include <map>
#include <queue>
using namespace std;

//this struct contains everything needed to use an Adjacency Matrix. reused often
struct AdjMtx
{
    //the actual container for the adjacency matrix
    vector<vector<int> > mtx;

    //this dictionary is so that indices can be agnostic of the actual vertice value
    map<int, int> values;
};

//simple vertice pair representing an edge. reused often
struct Edge
{

    //the first vertice
    int a;

    //the second vertice
    int b;
};

//struct for a graph. A graph is represented with an adjcency matrix and
//a vector of edges
struct Graph
{
    //the number of vertices
    int V;

    //the Adjacency Matrix struct that goes with this graph.
    AdjMtx adjMtx;

    //the list of edges associated with this graph
    vector<Edge> edges;
};

/*
Header like declaration of functions here.  See comment above implementation for some information
*/
Graph *createGraph(int, vector<Edge>);
AdjMtx createAdjMtx(vector<Edge>, int);
vector<int> BFS(Graph *, int);
queue<int> getAdjVertices(int);
vector<vector<int> > DistanceMatrix(Graph *g);
bool graphIsConnected(vector<vector<int> > mat);
int getDiameter(Graph);

//this function creates the adj matrix based on list of edges
AdjMtx createAdjMtx(vector<Edge> edges, int verticeCount)
{

    AdjMtx adjMtx;
    int verticeCounter = 0;

    //intialize a dictionary to map verticeValues to an index for the adjMtx
    map<int, int> verticeToIndex;

    map<int, int> indexToVertice;

    //initialize a verticeCount x verticeCount size matrix
    vector<vector<int> > adjVector(verticeCount, vector<int>(verticeCount));



    //set all values initially to zero
    for (int x = 0; x < verticeCount; x++)
    {
        for (int y = 0; y < verticeCount; y++)
        {
            adjVector[x][y] = 0;
        }
    }



    //TODO:  improve comments here.  this may not be clear to others
    for (int index = 0; index < edges.size(); index++)
    {
        int aIndex;
        int bIndex;
        int verticeA = edges[index].a;
        int verticeB = edges[index].b;

        if (verticeToIndex.count(verticeA) == 0)
        {

            verticeToIndex[verticeA] = verticeCounter;
            indexToVertice[verticeCounter] = verticeA;
            aIndex = verticeCounter;
            verticeCounter++;
        }
        else
        {
            aIndex = verticeToIndex[verticeA];
        }

        if (verticeToIndex.count(verticeB) == 0)
        {

            verticeToIndex[verticeB] = verticeCounter;
            indexToVertice[verticeCounter] = verticeB;
            bIndex = verticeCounter;
            verticeCounter++;
        }
        else
        {
            bIndex = verticeToIndex[verticeB];
        }
        //cout<<"A: "<<aIndex<<" : "<<verticeA<<endl;
        //cout<<"B: "<<bIndex<<" : " <<verticeB<<endl;
        //set the adj mtx to know that these two vertices are connected
        adjVector[aIndex][bIndex] = 1;
        adjVector[bIndex][aIndex] = 1;
    }

    cout<<"Adjacency Matrix: "<<endl;
    //for debugging, print adj mtx
    for (int y = 0; y < verticeCount; y++)
    {
        for (int x = 0; x < verticeCount; x++)
        {
            cout << adjVector[x][y] << " ";
        }
        cout << endl;
    }

    adjMtx.mtx = adjVector;
    adjMtx.values = indexToVertice;

    return adjMtx;
}

//function to create a graph of V - vertices
Graph *createGraph(int V, vector<Edge> edges)
{



    //make empty struct, and store #vertices
    Graph *graph = new Graph;
    graph->V = V;

    // Set the edges
    graph->edges = edges;

    // Create and set the adjacency matrix
    AdjMtx adjMtx = createAdjMtx(edges, V);
    graph->adjMtx = adjMtx;



    // Return the new graph
    return graph;
}

/*
TODO - should be removed - operator  for printing queues
*/
ostream &operator<<(ostream &os, queue<int> myQ) //function header
{
    while (!myQ.empty()) //body
    {
        os << myQ.front() << " ";
        myQ.pop();
    }
    return os; // end of function
}

//returns an queue with the adjacent vertices
queue<int> getAdjVertices(AdjMtx adjMtx, int verticeIndex)
{

    //stores the all vertice's that are adjacent to verticeIndex
    queue<int> adjacentVertices;

    //
    //loop through a row/col in adjMtx, and add the connected ones to a vector
    //
    for (int index = 0; index < adjMtx.mtx[verticeIndex].size(); index++)
    {

        //if the the mtx[verticeIndex][index] = 1, then verticeIndex is connected
        if (adjMtx.mtx[verticeIndex][index] == 1)
        {

            //recall that index isn't the actual vertice value. to get the actual value
            //you have to use the mapping in AdjMtx.
            adjacentVertices.push(index);
        }
    }

    // Return the queue
    return adjacentVertices;
}

//this function generates the distance matrix, stores it in g
vector<int> BFS(Graph *g, int start)
{

    AdjMtx adjMtx = g->adjMtx;
    int verticeCount = g->V;

    //an array of bools telling if a vertice has been visited yet.
    //e.g. if isVisited[1] is true, then the second vertice has already been visited
    bool isVisit[verticeCount];

    //fill isvisit array with false
    for (int index = 0; index < verticeCount; index++)
    {
        isVisit[index] = false;
    }

    //breadth first search queue
    queue<int> visitQueue;

    //we're going to arbitrarily start with the given vertex
    visitQueue.push(start);
    vector<int> distanceArr;
    for(int i = 0; i < verticeCount; i++)
	    distanceArr.push_back(0);

    //while the visit queue isn't empty, keep visiting!
    while (!visitQueue.empty())
    {

        //get all adjacent vertices to the current node on the queue
        queue<int> nextVertices = getAdjVertices(adjMtx, visitQueue.front());
        isVisit[visitQueue.front()] = true;

        // Loop through all neigboring vertices
        while (!nextVertices.empty())
        {

            // Only handle them if they haven't been visited
            if (!isVisit[nextVertices.front()])
            {

                // Increment the distance (based on parent's distance) if it hasn't been set yet
                if (distanceArr[nextVertices.front()] <= 0)
                    distanceArr[nextVertices.front()] = distanceArr[visitQueue.front()] + 1;

                // Adjust the queues accordinngly
                visitQueue.push(nextVertices.front());
                nextVertices.pop();
            }

            // Simply remove the vertex if it has already been visited
            else
            {
                nextVertices.pop();
            }
        }

        // Remove the vertex we just handled
        visitQueue.pop();
    }

    // Set every distance value not visited to -1
    for(int i = 0; i < verticeCount; i++) {
        if(!isVisit[i]) {
            distanceArr[i] = -1;
        }
    }

    // Return the vector
    return distanceArr;
}

//this function gets the diameter
int getDiameter(Graph *graph){

    //holds current max distance value; begin at 0 (distance of a one node graph)
    int max = 0;

    //get the distanceMatrix to find the max distance
    vector<vector< int> > distanceMatrix = DistanceMatrix(graph);

    for(int j = 0; j < distanceMatrix.size(); j++){
        for(int i = 0; i < distanceMatrix[0].size(); i++){
            if(distanceMatrix[i][j] > max){
                max = distanceMatrix[i][j];
            }
        }
    }

    return max;
}

// Function used to get the distance matrix
vector<vector<int> > DistanceMatrix(Graph *g) {

    // Create a vector to store each row in
    vector<vector<int> > matrix;

    // Get each row of the matrix
    int verticeCount = g->V;
    for(int vert = 0; vert < verticeCount; vert++)
        matrix.push_back(BFS(g, vert));

    // Return the matrix
    return matrix;
}

// Function used to determine if the graph is connected
// i.e. the matrix contains a -1
bool graphIsConnected(vector<vector<int> > mat) {
    for(int i = 0; i < mat.size(); i++)
        for(int j = 0; j < mat[i].size(); j++)
            if(mat[i][j] < 0)
                return false;
    return true;
}

// Helper function used to print a distance matrix
void printMatrix(vector<vector<int> > mat) {
    for(int i = 0; i < mat.size(); i++) {
        for(int j = 0; j < mat[i].size(); j++) {

            // Replace all negative values with an "X"
            if(mat[i][j] >= 0)
                cout << mat[i][j] << " ";
            else
                cout << "X" << " ";
        }
        cout << endl;
    }
}

void Components(Graph* g){
  vector<vector<int> > mat=DistanceMatrix(g);
  int width=0;
  int i=0, j=0;
  int num=1;
  cout<<"Vertex set #"<<num<<" : ";
  while(j<mat.size()){
//cout<<"start loop"<<endl;

    if(mat[i][j]>=0){
//cout<<"after the first if"<<endl;
      if(mat[i][j] == 1){
//cout<<"after the second if"<<endl;
        cout<<g->adjMtx.values[j]<<"->"<<g->adjMtx.values[i]<<"  ";
      }j++;
    }else{
//cout<<"change the width"<<endl;
      width=j-1;
      i++;j=i;
      if(i==width){
        num++;
        cout<<endl;
        cout<<"Vertex set #"<<num<<" : ";
      }
    }

 }
}

// Helper function used to create an edge
Edge createEdge(int a, int b) {
    Edge temp;
    temp.a = a;
    temp.b = b;
    return temp;
}

int main()
{

    // Allocate the variables used in the instance
    int totalVertices;
    vector<Edge> edges;

    // Get the number of vertices from the user
    cout << "Input the Number of Vertices: ";
    cin >> totalVertices;

    // Have the user input each edge connection they desire
    Graph *graph;
    int a, b;
    while(true) {

        // Prompt the user for both vertices
        cout << "Input Next Edge (or -1 to Finish):" << endl;

        // Get the two vertices the  edge joins, exiting if either is negative
        cout << "Vertex 1: ";
        cin >> a;
        if(a < 0)
            break;
        cout << "Vertex 2: ";
        cin >> b;
        if(b < 0)
            break;

        // Add the edge to the graph
        edges.push_back(createEdge(a, b));
    }

    // Prompt indicating output of the program
    cout << "Program Output:" << endl;

    // Create the graph using the vector of edges
    graph = createGraph(totalVertices, edges);

    // Create the distance matrix
    vector<vector<int> > distMat = DistanceMatrix(graph);
    cout << "Distance Matrix:" << endl;
    printMatrix(distMat);

    // Determine if the graph is connected
    if(graphIsConnected(distMat)) {
        cout << "Graph is Connected" << endl;

        // TODO - Print diameter
        cout<<"Diameter: " << getDiameter(graph)<<endl;
    } else {
        cout << "Graph is Not Connected" << endl;
        Components(graph);
        // TODO - print connected components
    }

    cout<<endl;

}
